What I learned from this project:

- This was my first project with Cocos2d-x. I learned a tremendous amount of the Cocos syntax.
- I discovered the 'director'.
- I became familiar with Scenes and that they contain: sprites, labels, nodes, child, etc. 
- I learned that Cocos has a built in simulation of real world physics. I know I have only scratched the surface of this with my game, setting up collision detection.
- Cocos has an option to trigger events by touch on mobile
- I learned that programmers need to be extremely meticulous. Simple mistakes such as capitalizing the inital letter of a variable instead of following camelCase can lead to issues. Any typo will usually lead to compiling errors.
- I know I have a lot more to learn. I would love to become more familiar with Cocos and C++.


How I would like to Modify this project:

- I would like to add a scoring system that displays the current score of the player, as well as the high score.
- Adding audio would add another element to the game to increase the user's entertainment.
- Increasing the Pipe spawn frequency incrementally as the player progresses would increase difficulty.
- Adding 'Powerups' could add interesting effects. For example, the player picks up a floating object that could either decrease the size of the 'bird', or increase the gap between the pipe. 
